package io.pilllopl.dddmasterpiece;

import io.pilllopl.dddmasterpiece.availability.application.AvailabilityConfiguration;
import io.pilllopl.dddmasterpiece.availability.application.AvailabilityFacade;
import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.availability.infrastructure.InMemoryMasterPieceRepo;
import io.pilllopl.dddmasterpiece.common.DomainEventPublisher;
import io.pilllopl.dddmasterpiece.common.DomainException;
import io.pilllopl.dddmasterpiece.common.EmailSender;
import io.pilllopl.dddmasterpiece.reservation.ReservationConfiguration;
import io.pilllopl.dddmasterpiece.reservation.ReservationFacade;
import org.awaitility.Awaitility;
import org.awaitility.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.transaction.TestTransaction;

import javax.transaction.Transactional;

import static io.pilllopl.dddmasterpiece.availability.domain.OwnerId.newOne;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest(classes = {DddMasterpieceApplication.class})
@Transactional
class SellingIntegrationTest {

    public static final OwnerId OWNER_ID = newOne();

    @Autowired
    ReservationFacade reservationFacade;

    @Autowired
    AvailabilityFacade availabilityFacade;


    @Test
    void canSellWhenMasterpieceExists() {
        //given
        MasterpieceId masterpieceId = newMasterPiece();

        //when
        availabilityFacade.sellMasterpiece(masterpieceId, OWNER_ID);

        //then
        assertThat(availabilityFacade.findBy(masterpieceId)).get().matches(m -> m.isSoldTo(OWNER_ID));
    }

    @Test
    void xreservationIsCreated() {
        //given
        MasterpieceId masterpiece = newMasterPiece();

        //when
        availabilityFacade.sellMasterpiece(masterpiece, OWNER_ID);

        //then
        Awaitility
                .await()
                .atMost(Duration.TWO_SECONDS)
                .until(() -> hasReservation(OWNER_ID));
        assertThat(reservationFacade.listReservationsBy(OWNER_ID)).hasSize(1);
    }

    boolean hasReservation(OwnerId ownerId) {
        return reservationFacade.listReservationsBy(ownerId).size() == 1;
    }

    MasterpieceId newMasterPiece() {
        return availabilityFacade.createMasterpiece(MasterpieceId.newOne());
    }

    @Test
    void cantSellWhenMasterpieceDoesNotExists() {
        //expect
        assertThatExceptionOfType(DomainException.class)
                .isThrownBy(() ->         availabilityFacade.sellMasterpiece(MasterpieceId.newOne(), OWNER_ID));
    }
}


