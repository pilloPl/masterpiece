package io.pilllopl.dddmasterpiece.reservation;

import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static java.time.ZoneId.systemDefault;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ReservationFacadeTest {

    public static final MasterpieceId MASTERPIECE_ID = MasterpieceId.newOne();
    public static final MasterpieceId MASTERPIECE_ID_2 = MasterpieceId.newOne();
    public static final Instant NOW = LocalDate.of(1989, 12, 15).atStartOfDay(systemDefault()).toInstant();
    public static final Instant YESTERDAY = NOW.atZone(systemDefault()).minusDays(1).toInstant();
    public static final Instant TOMORROW = NOW.atZone(systemDefault()).plusDays(1).toInstant();
    public static final OwnerId OWNER_ID = OwnerId.newOne();
    public static final OwnerId OWNER_ID_2 = OwnerId.newOne();

    ReservationFacade facade = new ReservationFacade(new ReservationsDatabase());

    @Test
    void canAddReservation() {
        //when
        facade.addTemporalReservation(MASTERPIECE_ID, OWNER_ID, NOW);
        //and
        facade.addPerpetualReservation(MASTERPIECE_ID_2, OWNER_ID_2);

        //then
        assertThat(facade.listReservationsBy(OWNER_ID)).hasSize(1);
        assertThat(facade.listReservationsBy(OWNER_ID).get(0)).isEqualTo(new Reservation(MASTERPIECE_ID, false, NOW));
        assertThat(facade.listReservationsBy(OWNER_ID_2)).hasSize(1);
        assertThat(facade.listReservationsBy(OWNER_ID_2).get(0)).isEqualTo(new Reservation(MASTERPIECE_ID_2, true, null));
    }

    @Test
    void canListActiveReservations() {
        //when
        facade.addTemporalReservation(MASTERPIECE_ID, OWNER_ID, TOMORROW);
        facade.addTemporalReservation(MASTERPIECE_ID, OWNER_ID, YESTERDAY);
        facade.addTemporalReservation(MASTERPIECE_ID, OWNER_ID, YESTERDAY);
        facade.addPerpetualReservation(MASTERPIECE_ID_2, OWNER_ID);

        //then
        assertThat(facade.listNotExpiredReservationsBy(OWNER_ID, NOW)).hasSize(2);
    }


}