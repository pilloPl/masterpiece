package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.common.DomainException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;

import static io.pilllopl.dddmasterpiece.availability.domain.OwnerId.newOne;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest
class AvailabilityFacadeIntegrationTest {

    public static final OwnerId OWNER_ID = newOne();
    public static final Instant UNTIL = now();

    @Autowired
    AvailabilityFacade availabilityFacade;

    @Test
    void canReserveWhenMasterpieceExists() {
        //given
        MasterpieceId masterpieceId = availabilityFacade.createMasterpiece(MasterpieceId.newOne());

        //when
        availabilityFacade.reserveMasterpiece(masterpieceId, OWNER_ID, UNTIL);

        //then
        assertThat(availabilityFacade.findBy(masterpieceId)).get().matches(m -> m.isReservedBy(OWNER_ID, UNTIL));
    }

    @Test
    void cantReserveWhenMasterpieceDoesNotExists() {
        //expect
        assertThatExceptionOfType(DomainException.class)
                .isThrownBy(() -> availabilityFacade.reserveMasterpiece(MasterpieceId.newOne(), OWNER_ID, UNTIL));
    }
}


