package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.common.DomainException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;

import static io.pilllopl.dddmasterpiece.availability.domain.OwnerId.newOne;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest
class ReserveMasterpieceServiceIntegrationTest {

    public static final OwnerId OWNER_ID = newOne();
    public static final Instant UNTIL = now();

    @Autowired
    MasterpieceRepository masterpieceRepository;

    CreateMasterpieceService createMasterpieceService;
    ReserveMasterpieceService reserveMasterpieceService;

    @BeforeEach
    public void setup() {
         createMasterpieceService = new CreateMasterpieceService(masterpieceRepository);
         reserveMasterpieceService = new ReserveMasterpieceService(masterpieceRepository);
    }

    @Test
    void canReserveWhenMasterpieceExists() {
        //given
        Masterpiece masterpiece = newMasterPiece();

        //when
        reserveMasterpieceService.reserveMasterpiece(new ReserveCommand(OWNER_ID, UNTIL, masterpiece.getId()));

        //then
        assertThat(masterpieceRepository.loadBy(masterpiece.getId())).get().matches(m -> m.isReservedBy(OWNER_ID, UNTIL));
    }

    Masterpiece newMasterPiece() {
        return createMasterpieceService.createMasterpiece(new CreateCommand(MasterpieceId.newOne()));
    }

    @Test
    void cantReserveWhenMasterpieceDoesNotExists() {
        //expect
        assertThatExceptionOfType(DomainException.class)
                .isThrownBy(() -> reserveMasterpieceService.reserveMasterpiece(new ReserveCommand(OWNER_ID, UNTIL, MasterpieceId.newOne())));
    }
}


