package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.availability.infrastructure.InMemoryMasterPieceRepo;
import io.pilllopl.dddmasterpiece.common.DomainEventPublisher;
import io.pilllopl.dddmasterpiece.common.DomainException;
import io.pilllopl.dddmasterpiece.common.EmailSender;
import io.pilllopl.dddmasterpiece.reservation.ReservationConfiguration;
import io.pilllopl.dddmasterpiece.reservation.ReservationFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static io.pilllopl.dddmasterpiece.availability.domain.OwnerId.newOne;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(MockitoExtension.class)
class SellMasterpieceServiceTest {

    public static final OwnerId OWNER_ID = newOne();

    MasterpieceRepository masterpieceRepository = new InMemoryMasterPieceRepo();
    CreateMasterpieceService createMasterpieceService = new CreateMasterpieceService(masterpieceRepository);
    EmailSender emailSender;
    DomainEventPublisher domainEventPublisher;
    SellMasterpieceService sellMasterpieceService;

    @BeforeEach
    public void setup() {
        emailSender = Mockito.mock(EmailSender.class);
        domainEventPublisher = Mockito.mock(DomainEventPublisher.class);
        sellMasterpieceService = new SellMasterpieceService(masterpieceRepository, emailSender, domainEventPublisher);
    }

    @Test
    void canSellWhenMasterpieceExists() {
        //given
        Masterpiece masterpiece = newMasterPiece();

        //when
        sellMasterpieceService.sellMasterpiece(new SellMasterpieceCommand(OWNER_ID, masterpiece.getId()));

        //then
        assertThat(masterpieceRepository.loadBy(masterpiece.getId())).get().matches(m -> m.isSoldTo(OWNER_ID));
    }

    @Test
    void shouldSendAnEmailAfterSell() {
        //given
        Masterpiece masterpiece = newMasterPiece();

        //when
        sellMasterpieceService.sellMasterpiece(new SellMasterpieceCommand(OWNER_ID, masterpiece.getId()));

        //then
        Mockito.verify(emailSender).sendEmail("sales@uczciwy.lombard.com", "nie uwierzysz", "ktoś to kupił");
    }

    @Test
    void eventIsSent() {
        //given
        Masterpiece masterpiece = newMasterPiece();

        //when
        sellMasterpieceService.sellMasterpiece(new SellMasterpieceCommand(OWNER_ID, masterpiece.getId()));

        //then
        Mockito.verify(domainEventPublisher).publish(Mockito.isA(MasterpieceSold.class));

    }

    Masterpiece newMasterPiece() {
        return createMasterpieceService.createMasterpiece(new CreateCommand(MasterpieceId.newOne()));
    }

    @Test
    void cantSellWhenMasterpieceDoesNotExists() {
        //expect
        assertThatExceptionOfType(DomainException.class)
                .isThrownBy(() ->         sellMasterpieceService.sellMasterpiece(new SellMasterpieceCommand(OWNER_ID, MasterpieceId.newOne())));
    }
}


