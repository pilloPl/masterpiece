package io.pilllopl.dddmasterpiece.availability.ui;

import io.pilllopl.dddmasterpiece.DddMasterpieceApplication;
import io.pilllopl.dddmasterpiece.availability.application.AvailabilityFacade;
import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static java.util.UUID.fromString;
import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = DddMasterpieceApplication.class)
@AutoConfigureMockMvc
public class ReservationControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    AvailabilityFacade availabilityFacade;

    static final String MASTERPIECE_ID = "cef0cbf3-6458-4f13-a418-ee4d7e7505dd";
    static final String MASTERPIECE_ID_2 = "6ef5218b-bab5-4535-a6bb-2aba965a705a";
    static final String OWNER_ID = "57702e5f-52cf-4637-990c-4734b0e77a7b";
    static final String OWNER_ID_2 = "edcba81c-4ad1-4db1-bfcc-593cf6759cf6";

    @Test
    public void canBuyMasterpiece() throws Exception {
        //given
        availabilityFacade.createMasterpiece(MasterpieceId.of(fromString(MASTERPIECE_ID)));

        //when
        mockMvc.perform(post("http://localhost:" + port + "/reservations")
                .header("Content-Type", "application/json")
                .content("{\"masterpieceId\": \" " + MASTERPIECE_ID + " \", " +
                        "  \"ownerId\": \"" + OWNER_ID + "\", " +
                        "  \"days\": 0, " +
                        "  \"forever\": \"true\"}"))

                .andExpect(status().isOk());

        //then
        mockMvc.perform(get("http://localhost:" + port + "/reservations/" + OWNER_ID))
                .andExpect(MockMvcResultMatchers
                        .content()
                        .json("[{\"masterpieceId\": " + MASTERPIECE_ID + ",\"forever\":true,\"reservedUntil\":null}]"));
    }

    @Test
    public void canReserveMasterpiece() throws Exception {
        //given
        availabilityFacade.createMasterpiece(MasterpieceId.of(fromString(MASTERPIECE_ID_2)));

        //when
        mockMvc.perform(post("http://localhost:" + port + "/reservations")
                .header("Content-Type", "application/json")
                .content("{\"masterpieceId\": \" " + MASTERPIECE_ID_2 + " \", " +
                        "  \"ownerId\": \"" + OWNER_ID_2 + "\", " +
                        "  \"days\": 3, " +
                        "  \"forever\": \"false\"}"))

                .andExpect(status().isOk());

        //then
        mockMvc.perform(get("http://localhost:" + port + "/reservations/" + OWNER_ID_2))
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].forever", Matchers.is(false)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].masterpieceId", Matchers.is(MASTERPIECE_ID_2)));


    }



}