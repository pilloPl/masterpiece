package io.pilllopl.dddmasterpiece.availability.domain;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.common.DomainException;
import org.junit.jupiter.api.Test;

import java.time.*;

import static java.time.ZoneId.systemDefault;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class MasterpieceTest {

    public static final OwnerId OWNER = OwnerId.newOne();
    public static final OwnerId POTENTIAL_OWNER = OwnerId.newOne();

    public static final Instant today = LocalDate.of(1989, 12, 15).atStartOfDay(systemDefault()).toInstant();
    public static final Instant tillTomorrow = today.atZone(systemDefault()).plusDays(1).toInstant();
    public static final Instant tillYesterday = today.atZone(systemDefault()).minusDays(1).toInstant();

    public static final Clock clock = Clock.fixed(today, ZoneId.systemDefault());

    @Test
    void canReserveMasterpieceWhenNotSoldOrReservedForAnotherClient() {
        //given
        Masterpiece masterpiece = Masterpiece.newOne(clock);

        //when
        masterpiece.reserveFor(POTENTIAL_OWNER, tillTomorrow);

        //then
        assertThat(masterpiece.isReservedBy(POTENTIAL_OWNER, tillTomorrow)).isTrue();
    }

    @Test
    void canNotReserveMasterpieceWhenReservedByAnotherClient() {
        //given
        Masterpiece masterpiece = Masterpiece.reservedBy(OWNER, tillTomorrow, clock);

        //expect
        assertThatExceptionOfType(DomainException.class).isThrownBy(() -> masterpiece.reserveFor(POTENTIAL_OWNER, tillTomorrow));
        assertThat(masterpiece.isReservedBy(OWNER, tillTomorrow)).isTrue();
    }

    @Test
    void canNotReserveMasterpieceWhenSoldForAnotherClient() {
        //given
        Masterpiece masterpiece = Masterpiece.soldTo(OWNER, clock);

        //expect
        assertThatExceptionOfType(DomainException.class).isThrownBy(() -> masterpiece.reserveFor(POTENTIAL_OWNER, tillTomorrow));

        assertThat(masterpiece.isSoldTo(OWNER)).isTrue();
    }

    @Test
    void canReserveTheSameMasterpieceMultipleTimes() {
        //given
        Masterpiece masterpiece = Masterpiece.soldTo(POTENTIAL_OWNER, clock);

        //when
        masterpiece.reserveFor(POTENTIAL_OWNER, tillTomorrow);

        //then
        assertThat(masterpiece.isReservedBy(POTENTIAL_OWNER, tillTomorrow)).isTrue();
    }

    @Test
    void reservationExpiresAfterSomeTime() {
        //given
        Masterpiece masterpiece = Masterpiece.reservedBy(OWNER, tillYesterday, clock);

        //when
        masterpiece.reserveFor(POTENTIAL_OWNER, tillTomorrow);

        //then
        assertThat(masterpiece.isReservedBy(POTENTIAL_OWNER, tillTomorrow)).isTrue();
    }

    @Test
    void canSellMasterpieceWhenNotSoldOrReservedForAnotherClient() {
        //given
        Masterpiece masterpiece = Masterpiece.newOne(clock);

        //when
        masterpiece.sellTo(POTENTIAL_OWNER);

        //then
        assertThat(masterpiece.isSoldTo(POTENTIAL_OWNER)).isTrue();
    }

    @Test
    void canSellTheSameMasterpieceMultipleTimes() {
        //given
        Masterpiece masterpiece = Masterpiece.newOne(clock);

        //when
        masterpiece.sellTo(POTENTIAL_OWNER);

        //then
        assertThat(masterpiece.isSoldTo(POTENTIAL_OWNER)).isTrue();

    }

    @Test
    void cannotReserveMasterpieceIfItIsRemoved() {
        //given
        Masterpiece masterpiece = Masterpiece.removed(clock);

        //expect
        assertThatExceptionOfType(DomainException.class)
                .isThrownBy(() -> masterpiece.reserveFor(POTENTIAL_OWNER, tillTomorrow));
    }

    @Test
    void cannotSellMasterpieceIfItIsRemoved() {
        //given
        Masterpiece masterpiece = Masterpiece.removed(clock);

        //expect
        assertThatExceptionOfType(DomainException.class)
                .isThrownBy(() -> masterpiece.sellTo(POTENTIAL_OWNER));

    }
}