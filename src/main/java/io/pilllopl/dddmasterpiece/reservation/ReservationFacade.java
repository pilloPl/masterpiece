package io.pilllopl.dddmasterpiece.reservation;

import io.pilllopl.dddmasterpiece.availability.application.MasterpieceSold;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import org.springframework.context.event.EventListener;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ReservationFacade {

    final ReservationsDatabase reservationsDatabase;

    public ReservationFacade(ReservationsDatabase reservationsDatabase) {
        this.reservationsDatabase = reservationsDatabase;
    }

    @EventListener
    public void reactTo(MasterpieceSold masterpieceSold) {
        addPerpetualReservation(masterpieceSold.masterpieceToSell, masterpieceSold.ownerId);
    }

    public void addTemporalReservation(MasterpieceId masterpieceId, OwnerId ownerId, Instant till) {
        reservationsDatabase.add(ownerId, Reservation.of(masterpieceId, till));
    }

    public void addPerpetualReservation(MasterpieceId masterpieceId, OwnerId ownerId) {
        reservationsDatabase.add(ownerId, Reservation.fromSaleOf(masterpieceId));
    }

    public List<Reservation> listReservationsBy(OwnerId ownerId) {
        return reservationsDatabase.findAllBy(ownerId);
    }


    List<Reservation> listNotExpiredReservationsBy(OwnerId ownerId, Instant till) {
        return reservationsDatabase.findNotExpiredBy(ownerId, till);
    }
}

class ReservationsDatabase {

    final Map<OwnerId, List<Reservation>> reservations = new ConcurrentHashMap<>();

    void add(OwnerId ownerId, Reservation of) {
        reservations.compute(ownerId, (ownerId1, currentReservations) -> {
            if (currentReservations == null) {
                currentReservations = new ArrayList<>();
            }
            currentReservations.add(of);
            return currentReservations;
        });
    }

    List<Reservation> findAllBy(OwnerId ownerId) {
        return reservations.getOrDefault(ownerId, new ArrayList<>());
    }

    List<Reservation> findNotExpiredBy(OwnerId ownerId, Instant till) {
        return findAllBy(ownerId)
                .stream()
                .filter(r -> r.notExpiredTill(till))
                .collect(Collectors.toList());
    }
}

class Reservation {

    static Reservation fromSaleOf(MasterpieceId masterpieceId) {
        return new Reservation(masterpieceId, true, null);
    }

    static Reservation of(MasterpieceId masterpieceId, Instant till) {
        return new Reservation(masterpieceId, false, till);
    }

    public final UUID masterpieceId;
    public final boolean forever;
    public final Instant reservedUntil;

    Reservation(MasterpieceId masterpieceId, boolean forever, Instant until) {
        this.masterpieceId = masterpieceId.uuid();
        this.forever = forever;
        this.reservedUntil = until;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Reservation that = (Reservation) o;
        return forever == that.forever &&
                Objects.equals(masterpieceId, that.masterpieceId) &&
                Objects.equals(reservedUntil, that.reservedUntil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(masterpieceId, forever, reservedUntil);
    }

    boolean notExpiredTill(Instant till) {
        return reservedUntil == null || !reservedUntil.isBefore(till);
    }
}