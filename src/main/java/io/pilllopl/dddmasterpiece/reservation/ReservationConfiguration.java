package io.pilllopl.dddmasterpiece.reservation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReservationConfiguration {

    @Bean
    public ReservationFacade reservationFacade() {
        return new ReservationFacade(new ReservationsDatabase());
    }
}
