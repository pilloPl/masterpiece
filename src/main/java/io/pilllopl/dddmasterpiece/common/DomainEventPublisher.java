package io.pilllopl.dddmasterpiece.common;

public interface DomainEventPublisher {

    void publish(DomainEvent domainEvent);

}

