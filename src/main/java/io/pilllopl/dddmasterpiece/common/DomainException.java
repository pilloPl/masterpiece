package io.pilllopl.dddmasterpiece.common;

public class DomainException extends RuntimeException {
    public DomainException(String msg) {
        super(msg);
    }
}
