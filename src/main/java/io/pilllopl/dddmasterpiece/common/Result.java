package io.pilllopl.dddmasterpiece.common;

public class Result {

    private enum Type {
        Success, Failure
    }

    public static Result failure(String msg) {
        return new Result(msg, Type.Failure);
    }

    public Result failure() {
        return failure("");
    }

    public Result success(String msg) {
        return new Result(msg, Type.Success);
    }

    public Result success() {
        return success("");
    }

    private final String msg;
    private final Type type;

    private Result(String msg, Type type) {
        this.msg = msg;
        this.type = type;
    }

    public boolean isSuccess() {
        return type.equals(Type.Success);
    }

    public boolean isFailure() {
        return type.equals(Type.Failure);
    }
}
