package io.pilllopl.dddmasterpiece.availability.domain;

import io.pilllopl.dddmasterpiece.common.DomainException;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.time.Clock;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import static io.pilllopl.dddmasterpiece.availability.domain.Ownership.noOwnerShip;
import static java.util.UUID.randomUUID;

@Entity
public class Masterpiece {

    public static Masterpiece reservedBy(OwnerId ownerId, Instant till, Clock clock) {
        return new Masterpiece(ownerId, false, till, false, clock);
    }

    public static Masterpiece soldTo(OwnerId ownerId, Clock clock) {
        return new Masterpiece(ownerId, true, null, false, clock);
    }

    public static Masterpiece newOne(Clock clock, MasterpieceId id) {
        return new Masterpiece(id,null, false, null, false, clock);
    }

    public static Masterpiece newOne(Clock clock) {
        return new Masterpiece(null, false, null, false, clock);
    }

    public static Masterpiece removed(Clock clock) {
        return new Masterpiece(null, false, null, true, clock);
    }

    public Masterpiece() {

    }

    @EmbeddedId
    private MasterpieceId masterpieceId;

    private boolean isRemoved;

    @Transient
    private Clock clock = Clock.systemDefaultZone();

    @Embedded
    private Ownership ownership = noOwnerShip();

    public Masterpiece(OwnerId reservedBy, boolean isReservedPerpetually, Instant reservedUntil, boolean isRemoved, Clock clock) {
        this.clock = clock;
        this.masterpieceId = MasterpieceId.newOne();
        this.ownership = Ownership.of(reservedBy, reservedUntil, isReservedPerpetually);
        this.isRemoved = isRemoved;
    }

    public Masterpiece(MasterpieceId masterpieceId, OwnerId reservedBy, boolean isReservedPerpetually, Instant reservedUntil, boolean isRemoved, Clock clock) {
        this.masterpieceId = masterpieceId;
        this.clock = clock;
        this.ownership = Ownership.of(reservedBy, reservedUntil, isReservedPerpetually);
        this.isRemoved = isRemoved;
    }

    public void reserveFor(OwnerId potentialOwner, Instant till) {
        if (isRemoved) {
            throw new DomainException("Cannot reserve sold masterpiece, because it is removed: " + masterpieceId);
        }
        if (ownership.isReservedBySomeoneElseThan(clock, potentialOwner)) {
            throw new DomainException("Cannot reserve sold masterpiece, because is reserved for someone else: " + masterpieceId);
        }
        this.ownership = Ownership.reservedBy(potentialOwner, till);
    }

    void cancelReservationBy(OwnerId potentialOwner) {
        if (ownership.canCancel(clock, potentialOwner)) {
            throw new DomainException("Cannot cancel reservation: " + masterpieceId);
        }
        this.ownership = Ownership.noOwnerShip();
    }

    public void sellTo(OwnerId potentialOwner) {
        if (isRemoved) {
            throw new DomainException("Cannot reserve sold masterpiece, because it is removed: " + masterpieceId);
        }
        if (ownership.isReservedBySomeoneElseThan(clock, potentialOwner)) {
            throw new DomainException("Cannot cancel reservation, because it is owned by someone else: " + masterpieceId);
        }
        this.ownership = ownership.soldTo(potentialOwner);
    }

    void remove() {
        isRemoved = true;
    }

    public boolean isReservedBy(OwnerId ownerId, Instant until) {
        return ownership.isReservedBy(ownerId, until);
    }

    public boolean isSoldTo(OwnerId ownerId) {
        return ownership.isSoldTo(ownerId);
    }


    public MasterpieceId getId() {
        return masterpieceId;
    }
}

