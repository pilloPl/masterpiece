package io.pilllopl.dddmasterpiece.availability.domain;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.time.Clock;
import java.time.Instant;

@Embeddable
public class Ownership {

    public Ownership() {

    }

    @Embedded
    private OwnerId reservedBy;
    private boolean isReservedPerpetually;
    private Instant reservedUntil;

    private Ownership(OwnerId reservedBy, boolean isReservedPerpetually, Instant reservedUntil) {
        this.reservedBy = reservedBy;
        this.isReservedPerpetually = isReservedPerpetually;
        this.reservedUntil = reservedUntil;
    }

    static Ownership soldTo(OwnerId owner) {
        return new Ownership(owner, true, null);
    }
    static Ownership reservedBy(OwnerId owner, Instant till) {
        return new Ownership(owner, false, till);
    }

    static Ownership noOwnerShip() {
        return new Ownership(null, false, null);
    }

    static Ownership of(OwnerId reservedBy, Instant reservedUntil, boolean isReservedPerpetually) {
        return new Ownership(reservedBy, isReservedPerpetually, reservedUntil);
    }

    boolean canCancel(Clock clock, OwnerId potentialOwner) {
        if (isReservedPerpetually) {
            return false;
        }
        if (noReservation() || reservationHasExpired(clock)) {
            return true;
        }
        if (!reservedBy.equals(potentialOwner)) {
            return false;
        }
        return true;
    }

    private boolean noReservation() {
        return reservedBy == null;
    }

    boolean isSoldTo(OwnerId ownerId) {
        return isReservedPerpetually && ownerId.equals(reservedBy);
    }

    boolean isReservedBy(OwnerId ownerId, Instant until) {
        return reservedBy != null && reservedBy.equals(ownerId) && reservedUntil != null && until.equals(reservedUntil);
    }

    boolean isReservedBySomeoneElseThan(Clock clock, OwnerId potentialOwner) {
        if (isReservedPerpetually) {
            return !reservedBy.equals(potentialOwner);
        }
        if (noReservation() || reservationHasExpired(clock)) {
            return false;
        }
        return !reservedBy.equals(potentialOwner);
    }

    private boolean reservationHasExpired(Clock clock) {
        return Instant.now(clock).isAfter(reservedUntil);
    }

    Ownership sellTo(OwnerId potentialOwner) {
        return Ownership.soldTo(potentialOwner);
    }

}