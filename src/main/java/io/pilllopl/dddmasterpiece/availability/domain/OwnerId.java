package io.pilllopl.dddmasterpiece.availability.domain;

import javax.persistence.Embeddable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import static java.util.UUID.randomUUID;

@Embeddable
public class OwnerId {

    public OwnerId() { }

    public static OwnerId newOne() {
        return new OwnerId(randomUUID());
    }

    private UUID ownerId;

    OwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public static OwnerId of(UUID ownerId) {
        return new OwnerId(ownerId);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final OwnerId ownerId1 = (OwnerId) o;
        return Objects.equals(ownerId, ownerId1.ownerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ownerId);
    }
}
