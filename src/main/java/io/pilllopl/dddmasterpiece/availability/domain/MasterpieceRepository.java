package io.pilllopl.dddmasterpiece.availability.domain;

import java.util.Optional;

public interface MasterpieceRepository {

    Optional<Masterpiece> loadBy(MasterpieceId masterpieceId);

    Masterpiece save(MasterpieceId id, Masterpiece masterpiece);

}
