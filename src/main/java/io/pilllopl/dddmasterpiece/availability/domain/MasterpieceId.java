package io.pilllopl.dddmasterpiece.availability.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import static java.util.UUID.randomUUID;

@Embeddable
public class MasterpieceId implements Serializable {

    public MasterpieceId() {
    }

    public static MasterpieceId newOne() {
        return new MasterpieceId(randomUUID());
    }

    private UUID masterpieceId;

    MasterpieceId(UUID masterpieceId) {
        this.masterpieceId = masterpieceId;
    }

    public static MasterpieceId of(UUID masterpiece) {
        return new MasterpieceId(masterpiece);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final MasterpieceId ownerId1 = (MasterpieceId) o;
        return Objects.equals(masterpieceId, ownerId1.masterpieceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(masterpieceId);
    }

    public UUID uuid() {
        return masterpieceId;
    }
}
