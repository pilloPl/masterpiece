package io.pilllopl.dddmasterpiece.availability.infrastructure;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface JpaMasterpieceRepository extends MasterpieceRepository, JpaRepository<Masterpiece, MasterpieceId> {

    default Optional<Masterpiece> loadBy(MasterpieceId masterpieceId) {
        return findById(masterpieceId);
    }

    default Masterpiece save(MasterpieceId id, Masterpiece masterpiece) {
        return this.save(masterpiece);
    }
}
