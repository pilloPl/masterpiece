package io.pilllopl.dddmasterpiece.availability.infrastructure;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemoryMasterPieceRepo implements MasterpieceRepository {

    final Map<MasterpieceId, Masterpiece> masterpieces = new HashMap<>();

    @Override
    public Optional<Masterpiece> loadBy(MasterpieceId masterpieceId) {
        return Optional.ofNullable(masterpieces.get(masterpieceId));
    }

    @Override
    public Masterpiece save(MasterpieceId id, Masterpiece masterpiece) {
        masterpieces.put(id, masterpiece);
        return masterpiece;
    }
}
