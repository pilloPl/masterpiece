package io.pilllopl.dddmasterpiece.availability.ui;

import io.pilllopl.dddmasterpiece.availability.application.AvailabilityFacade;
import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.reservation.ReservationFacade;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

@RestController
class ReservationController {

    private final AvailabilityFacade availabilityFacade;
    private final ReservationFacade reservationFacade;
    private final Clock clock = Clock.systemDefaultZone();

    ReservationController(AvailabilityFacade availabilityFacade, ReservationFacade reservationFacade) {
        this.availabilityFacade = availabilityFacade;
        this.reservationFacade = reservationFacade;
    }

    @PostMapping("/reservations")
    ResponseEntity buy(@RequestBody ReserveRequest reserveRequest) {
        return ResponseEntity.notFound().build();

    }

    @GetMapping("/reservations/{ownerId}")
    ResponseEntity reserve(@PathVariable UUID ownerId) {
        return ResponseEntity.notFound().build();
    }

    private Instant fromToday(int days) {
        return Instant.now(clock).atZone(ZoneId.systemDefault()).plusDays(days).toInstant();
    }

}


class ReserveRequest {

    public UUID masterpieceId;
    public UUID ownerId;
    public int days;
    public boolean forever;

}
