package io.pilllopl.dddmasterpiece.availability.application;


import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;

import java.time.Instant;
import java.util.Optional;

public class AvailabilityFacade {

    private final CreateMasterpieceService createMasterpieceService;
    private final ReserveMasterpieceService reserveMasterpieceService;
    private final SellMasterpieceService sellMasterpieceService;
    private final MasterpieceRepository masterpieceRepository;

    public AvailabilityFacade(CreateMasterpieceService createMasterpieceService, ReserveMasterpieceService reserveMasterpieceService, SellMasterpieceService sellMasterpieceService, MasterpieceRepository masterpieceRepository) {
        this.createMasterpieceService = createMasterpieceService;
        this.reserveMasterpieceService = reserveMasterpieceService;
        this.sellMasterpieceService = sellMasterpieceService;
        this.masterpieceRepository = masterpieceRepository;
    }

    public MasterpieceId createMasterpiece(MasterpieceId masterpieceid) {
        return createMasterpieceService.createMasterpiece(new CreateCommand(masterpieceid)).getId();
    }

    public void reserveMasterpiece(MasterpieceId masterpieceid, OwnerId ownerId, Instant untill) {
        reserveMasterpieceService.reserveMasterpiece(new ReserveCommand(ownerId, untill, masterpieceid));
    }

    public Optional<Masterpiece> findBy(MasterpieceId masterpieceId) {
        return masterpieceRepository.loadBy(masterpieceId);
    }

    public void sellMasterpiece(MasterpieceId masterpieceId, OwnerId ownerId) {
        sellMasterpieceService.sellMasterpiece(new SellMasterpieceCommand(ownerId, masterpieceId));
    }
}
