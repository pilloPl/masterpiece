package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.common.DomainEvent;

public class MasterpieceSold implements DomainEvent {

    public final MasterpieceId masterpieceToSell;
    public final OwnerId ownerId;

    public MasterpieceSold(MasterpieceId masterpieceToSell, OwnerId ownerId) {
        this.masterpieceToSell = masterpieceToSell;
        this.ownerId = ownerId;
    }
}
