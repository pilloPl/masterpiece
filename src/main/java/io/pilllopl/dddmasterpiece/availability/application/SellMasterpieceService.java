package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.common.DomainEventPublisher;
import io.pilllopl.dddmasterpiece.common.DomainException;
import io.pilllopl.dddmasterpiece.common.EmailSender;
import io.pilllopl.dddmasterpiece.reservation.ReservationFacade;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.transaction.Transactional;

public class SellMasterpieceService {

    private final MasterpieceRepository masterpieceRepository;
    private final EmailSender emailSender;
    private final DomainEventPublisher domainEventPublisher;

    SellMasterpieceService(MasterpieceRepository masterpieceRepository, EmailSender emailSender, DomainEventPublisher domainEventPublisher) {
        this.masterpieceRepository = masterpieceRepository;
        this.emailSender = emailSender;
        this.domainEventPublisher = domainEventPublisher;
    }

    @Transactional
    public void sellMasterpiece(SellMasterpieceCommand cmd) {
        Masterpiece masterpiece = masterpieceRepository.loadBy(cmd.masterpieceToSell)
                .orElseThrow(() -> new DomainException("Cannot find masterpiece with id: " + cmd.masterpieceToSell));
        masterpiece.sellTo(cmd.ownerId);
        masterpieceRepository.save(cmd.masterpieceToSell, masterpiece);
        domainEventPublisher.publish(new MasterpieceSold(cmd.masterpieceToSell, cmd.ownerId));
        emailSender.sendEmail("sales@uczciwy.lombard.com", "nie uwierzysz", "ktoś to kupił");
    }
}

class SellMasterpieceCommand {
    final OwnerId ownerId;
    final MasterpieceId masterpieceToSell;

    SellMasterpieceCommand(OwnerId ownerId, MasterpieceId masterpieceToSell) {
        this.ownerId = ownerId;
        this.masterpieceToSell = masterpieceToSell;
    }


}
