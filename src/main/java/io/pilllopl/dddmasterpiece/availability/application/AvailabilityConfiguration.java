package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import io.pilllopl.dddmasterpiece.availability.infrastructure.InMemoryMasterPieceRepo;
import io.pilllopl.dddmasterpiece.common.DomainEventPublisher;
import io.pilllopl.dddmasterpiece.common.EmailSender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AvailabilityConfiguration {

    AvailabilityFacade withInMemoryDatabase() {
        return availabilityFacade(new InMemoryMasterPieceRepo(), new NoOpDomainEventPublisher());
    }

    @Bean
    AvailabilityFacade availabilityFacade(MasterpieceRepository masterpieceRepository, DomainEventPublisher domainEventPublisher) {
        return new AvailabilityFacade(new CreateMasterpieceService(masterpieceRepository), new ReserveMasterpieceService(masterpieceRepository), new SellMasterpieceService(masterpieceRepository, new EmailSender(), domainEventPublisher), masterpieceRepository);
    }


}
