package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;

import java.time.Clock;

class CreateMasterpieceService {

    private final MasterpieceRepository masterpieceRepository;
    private final Clock clock;

    CreateMasterpieceService(MasterpieceRepository masterpieceRepository) {
        this.masterpieceRepository = masterpieceRepository;
        this.clock = Clock.systemDefaultZone();
    }

    Masterpiece createMasterpiece(CreateCommand cmd) {
        Masterpiece masterpiece = Masterpiece.newOne(clock, cmd.id);
        return masterpieceRepository.save(masterpiece.getId(), masterpiece);
    }
}

class CreateCommand {

    MasterpieceId id;

    CreateCommand(MasterpieceId id) {
        this.id = id;
    }

}
