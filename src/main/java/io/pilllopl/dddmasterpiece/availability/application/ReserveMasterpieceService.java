package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.availability.domain.Masterpiece;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceId;
import io.pilllopl.dddmasterpiece.availability.domain.MasterpieceRepository;
import io.pilllopl.dddmasterpiece.availability.domain.OwnerId;
import io.pilllopl.dddmasterpiece.common.DomainEventPublisher;
import io.pilllopl.dddmasterpiece.common.DomainException;

import java.time.Instant;

public class ReserveMasterpieceService {

    private final MasterpieceRepository masterpieceRepository;

    ReserveMasterpieceService(MasterpieceRepository masterpieceRepository) {
        this.masterpieceRepository = masterpieceRepository;
    }

    void reserveMasterpiece(ReserveCommand cmd) {
        Masterpiece masterpiece = masterpieceRepository.loadBy(cmd.masterpieceToReserve)
                .orElseThrow(() -> new DomainException("Cannot find masterpiece with id: " + cmd.masterpieceToReserve));
        masterpiece.reserveFor(cmd.ownerId, cmd.until);
        masterpieceRepository.save(cmd.masterpieceToReserve, masterpiece);
    }
}

class ReserveCommand {
    final OwnerId ownerId;
    final Instant until;
    final MasterpieceId masterpieceToReserve;

    ReserveCommand(OwnerId ownerId, Instant until, MasterpieceId toReserve) {
        this.ownerId = ownerId;
        this.until = until;
        this.masterpieceToReserve = toReserve;
    }


}
