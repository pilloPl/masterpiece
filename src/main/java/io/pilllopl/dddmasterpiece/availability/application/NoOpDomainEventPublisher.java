package io.pilllopl.dddmasterpiece.availability.application;

import io.pilllopl.dddmasterpiece.common.DomainEvent;
import io.pilllopl.dddmasterpiece.common.DomainEventPublisher;

public class NoOpDomainEventPublisher implements DomainEventPublisher {
    @Override
    public void publish(DomainEvent domainEvent) {

    }
}
