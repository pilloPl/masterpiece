package io.pilllopl.dddmasterpiece;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DddMasterpieceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DddMasterpieceApplication.class, args);
	}

}
